#ifndef MYSERIAL_H
#define MYSERIAL_H

#include <QObject>
#include <QList>
#include <QSerialPort>
#include "structs.h"

class MySerial : public QObject
{
    Q_OBJECT
public:
    explicit MySerial(QObject *parent = nullptr);

    ~MySerial();

    /* A simple function to send (write) a ByteArray (data) over a serial
     * device. */
    void writeData(const QByteArray &data);

    /* A function to show a String "message" in the Application Output console.
     * See the below (bottom border) tabs in QtCreator. */
    void showStatusMessage(const QString &message);

    /* Calculates the checksum16. This is identical to the function on the
     * embedded code. */
    uint16_t chksum_calc();

    /* The main function of this class. It processes the received buffers by a
     * serial port. */
    void process_buffer();

signals:
   /* This signal is connected to the MainWindow dataHandler slot. It is used to
    * signalizes the dataHandler slot (function) whenever there are new data to
    * be shown on SensorsGroupBox fields.  */
    void newData(sensors_t sensors);

public slots:
    /* Slots (functions) that are connected to some signal. */

    /* The openSerialPort is connected to the Connect pushbutton. So, whenever
     * the connect pushbutton is clicked, this function is called. */
    void openSerialPort();

    /* In a similar way, the closeSerialPort is connected to the Disconnect
     * pushbutton. */
    void closeSerialPort();

    /* The readData slot (function) is connected to a signal of the QSerialPort
     * class. Such signal is called readyRead and it is emitted every time
     * new data is available for reading in the respective serial port. */
    void readData();

private:
    /* A QSerialPort object, which by the way will represent our serial port
     * device. */
    QSerialPort *m_p_serial;

    /* A ByteArray which will be used as a buffer of received data from the
     * serial port. */
    QByteArray m_buffer;
};

#endif // MYSERIAL_H
