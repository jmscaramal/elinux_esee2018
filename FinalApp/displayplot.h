#ifndef DISPLAYPLOT_H
#define DISPLAYPLOT_H

#include <qwt/qwt_plot.h>
#include <qwt/qwt_compat.h>
#include <qwt/qwt_legend.h>
#include <qwt/qwt_plot_curve.h>
#include <qwt/qwt_plot_grid.h>
#include <qwt/qwt_scale_draw.h>
#include <QTime>
#include <QList>

class DisplayPlot : public QwtPlot
{
    Q_OBJECT


public:

    DisplayPlot(QString title, QString unit);
    void updatePlot(QVector<double> Lead_II);
    QString getTitle();
    QString getUnit();


private:

    QString m_title;
    QString m_unit;

    QwtPlotGrid  *m_p_Grid;
    QwtPlotCurve *m_p_Lead_II;

};

#endif // DISPLAYPOT_H
