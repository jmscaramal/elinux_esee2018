#ifndef STRUCTS_H
#define STRUCTS_H

/* The following structs are identical to the ones defined in the embedded
 * software. */

typedef struct gyros_t
{
    uint32_t count;

    float gyro_x;
    float gyro_x_temp;

    float gyro_y;
    float gyro_y_temp;

    float gyro_z;
    float gyro_z_temp;

    float time_ms;
}gyros_t;  // 32 bytes

typedef struct mdan_t
{
    uint32_t count;

    float accel_x;
    float accel_x_temp;
    float vf_x_temp;

    float accel_y;
    float accel_y_temp;
    float vf_y_temp;

    float accel_z;
    float accel_z_temp;
    float vf_z_temp;

    float v_ad_5;
    float v_ad_15;
    float v_ad_m15;
    float v_reg_5;

    float time_ms;
}mdan_t; // 60 bytes

typedef struct sensors_t
{
    uint32_t count;
    mdan_t mdan;
    gyros_t gyros;
}sensors_t;

#endif // STRUCTS_H
