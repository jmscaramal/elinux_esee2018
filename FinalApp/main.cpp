#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    /* The QApplication class takes care of inputs arguments, among other
     * things, but mainly of the event loop, which by the way waits for user
     * input (interaction) with your GUI application. */
    QApplication a(argc, argv);

    /* As the name says, the MainWindow class is the "main window". The
     * following lines creates and shows, respectively, the main window.
     * All the setup related to such window, as buttons, texts and any other
     * visual object, should be defined in the MainWindow files (.h and .cpp).*/
    MainWindow w;
    w.show();

    /* Calling your app.exec() launchs the event loop. */
    return a.exec();
}




