#include "myserial.h"
#include "structs.h"
#include <QDebug>

/* MySerial class constructor. */
MySerial::MySerial(QObject *parent) : QObject(parent)
{
    /* Initializes a new QSerialPort object. */
    m_p_serial = new QSerialPort(this);

    /* Connects the readyRead signal, which indicates whenever there are new
     * data avalable for reading in the m_p_serial port, to the slot (function)
     * readData. */
    connect(m_p_serial, SIGNAL(readyRead()), this, SLOT(readData()));
}

/* MySerial class destructor. Empties the buffer and closes the serial port. */
MySerial::~MySerial()
{
    m_buffer.clear();
    closeSerialPort();
}

/* Opens the serial port "/dev/ttyUSB0" with the following parameters.*/
void MySerial::openSerialPort()
{
    m_p_serial->setPortName("/dev/ttyUSB0");
    m_p_serial->setBaudRate(115200);
    m_p_serial->setDataBits(QSerialPort::Data8);
    m_p_serial->setParity(QSerialPort::NoParity);
    m_p_serial->setStopBits(QSerialPort::OneStop);
    m_p_serial->setFlowControl(QSerialPort::NoFlowControl);

    /* Tries to open the device with Read and Write permissions. */
    if (m_p_serial->open(QIODevice::ReadWrite))
    {
        showStatusMessage("Serial connected");
    }
    else
    {
        showStatusMessage(tr("Opening Serial error"));
    }
}

/* Closes the serial port. */
void MySerial::closeSerialPort()
{
    /* If the m_p_serial port is opened, then closes it. */
    if (m_p_serial->isOpen())
        m_p_serial->close();

    showStatusMessage(tr("Disconnected"));
}

/* Writes the "data" ByteArray to the m_p_serial port. */
void MySerial::writeData(const QByteArray &data)
{
    m_p_serial->write(data);
}

/* This function is called whenever there are new data to be read from the
 * m_p_serial port. */
void MySerial::readData()
{
    /* Displays the number of available bytes to be read. */
   qDebug() << "Bytes available " << m_p_serial->bytesAvailable();

   /* Reads the available bytes from the m_p_serial port and appends the read
    * bytes on the m_buffer. */
   m_buffer.append(m_p_serial->readAll());

   /* The function process_buffer is only called if there are at least 102 bytes
    * on the m_bufer, which is the size of a telemetry frame. */
   if(m_buffer.size() >= 102)
   {
       process_buffer();
   }
}

void MySerial::showStatusMessage(const QString &message)
{
    qDebug() << message;
}

/* The same function from the embedded code. For further details, please check
 * the embedded source code. */
uint16_t MySerial::chksum_calc()
{
    uint16_t sum_a = 0xff;
    uint16_t sum_b = 0xff;
    uint16_t frame_size;
    unsigned short index;

    frame_size = (uint8_t)m_buffer[3] - 3;

    for( index = frame_size; index > 0; index--)
    {
        sum_a += (uint8_t)m_buffer[index];
        sum_b += sum_a;
    }

    sum_a += (uint8_t)m_buffer[index];
    sum_b += sum_a;

    sum_a = (sum_a & 0xff) + (sum_a >> 8);
    sum_b = (sum_b & 0xff) + (sum_b >> 8);

    sum_a &= 0xFF;
    sum_b &= 0xFF;

    return ((sum_a << 8) | sum_b);
}

/* As the name suggests, this function processes the current bytes on m_buffer.
 */
void MySerial::process_buffer()
{
    uint16_t index = 0;
    uint8_t  frame_size;
    sensors_t sensors;

    /* Starts a loop which walks through each available byte on m_buffer. */
    while (index < m_buffer.size())
    {
        /* Checks if the current byte represents the beginning of a telemetry
         * frame: 0xAB = 171 (int). */
        if ((uint8_t)m_buffer[index] == 171)
        {
            /* When the first byte is found, checks if the next one is the
             * second part of the beginning of the telemetry frame:
             * 0x55 = 85 (int). */
            if ((uint8_t)m_buffer[index+1] == 85)
            {
                /* Once the beginning of a telemetry frame is found, discards
                 * every previous byte (from 0 to index) present on m_buffer.*/
                m_buffer.remove(0, index);

                /* Computes a chksum with the remaining bytes (in m_buffer).
                 * Since the m_buffer variable and the chksum_calc function are
                 * members of the same class (MySerial), there is no need to
                 * pass the m_buffer as argument. */
                uint16_t chksum = chksum_calc();

                /* Then gets the frame_size, 3rd byte in the frame. */
                frame_size = (uint8_t)m_buffer[3];

                /* And mounts the chksum sent in the last two bytes of the
                 * telemetry frame. (see the embedded code) */
                uint16_t conf_chk = (uint8_t)m_buffer[frame_size - 2];
                conf_chk = conf_chk << 8;
                conf_chk = conf_chk + (uint8_t)m_buffer[frame_size - 1];

                /* If the computed chksum is the same as the one sent in the
                 * last 2 bytes of the telemetry frame, then it means that a
                 * full (and correct) telemetry frame was found. */
                if (chksum == conf_chk)
                {
                    /* So, it is time to composes it, that is, to mount each
                     * variable according to the way they were decomposed in the
                     * embedded code. */
                    char *data = m_buffer.data();

                    memcpy(&sensors.mdan.count, &data[4], sizeof(uint32_t));
                    memcpy(&sensors.mdan.accel_x, &data[8], sizeof(float));
                    memcpy(&sensors.mdan.accel_x_temp, &data[12], sizeof(float));
                    memcpy(&sensors.mdan.vf_x_temp, &data[16], sizeof(float));
                    memcpy(&sensors.mdan.accel_y, &data[20], sizeof(float));
                    memcpy(&sensors.mdan.accel_y_temp, &data[24], sizeof(float));
                    memcpy(&sensors.mdan.vf_y_temp, &data[28], sizeof(float));
                    memcpy(&sensors.mdan.accel_z, &data[32], sizeof(float));
                    memcpy(&sensors.mdan.accel_z_temp, &data[36], sizeof(float));
                    memcpy(&sensors.mdan.vf_z_temp, &data[40], sizeof(float));
                    memcpy(&sensors.mdan.v_ad_5, &data[44], sizeof(float));
                    memcpy(&sensors.mdan.v_reg_5, &data[48], sizeof(float));
                    memcpy(&sensors.mdan.v_ad_15, &data[52], sizeof(float));
                    memcpy(&sensors.mdan.v_ad_m15, &data[56], sizeof(float));
                    memcpy(&sensors.mdan.time_ms, &data[60], sizeof(float));
                    memcpy(&sensors.gyros.count, &data[64], sizeof(uint32_t));
                    memcpy(&sensors.gyros.gyro_x, &data[68], sizeof(float));
                    memcpy(&sensors.gyros.gyro_x_temp, &data[72], sizeof(float));
                    memcpy(&sensors.gyros.gyro_y, &data[76], sizeof(float));
                    memcpy(&sensors.gyros.gyro_y_temp, &data[80], sizeof(float));
                    memcpy(&sensors.gyros.gyro_z, &data[84], sizeof(float));
                    memcpy(&sensors.gyros.gyro_z_temp, &data[88], sizeof(float));
                    memcpy(&sensors.gyros.time_ms, &data[92], sizeof(float));
                    memcpy(&sensors.count, &data[96], sizeof(uint32_t));

                    /* Empties the composed telemetry frame from m_buffer. */
                    m_buffer.remove(0, frame_size);

                    /* And finally, emits the signal newData passing the sensors
                     * data structure as argument. Which by the way, will be the
                     * same data structure received by the dataHandler slot, in
                     * the MainWindow class. */
                    emit newData(sensors);
               }
            }
        }
        index++;
    }
}
