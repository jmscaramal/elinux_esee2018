#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "myserial.h"
#include "qcustomplot.h"

#include <QMainWindow>
#include <QGridLayout>
#include <QGroupBox>
#include <QPushButton>
#include <QTextBrowser>
#include <QSerialPort>
#include <QLineEdit>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    /* This function (slot) is responsible for putting all the data present on
     * sensors structure in their respective fields at SensorsGroupBox. */
    void DataHandler(sensors_t sensors);

private:
    Ui::MainWindow *ui;

    /*  Functions declaration. The following are responsible for creating the
     *  respective boxes in the mainwindow. See their definitions in
     *  MainWindow.cpp. */
    void createMenuGroupBox();
    void createLogGroupBox();
    void createSensorsGroupBox();
    void createPlotGroupBox();

    /*  Main menu GroupBox variables declaration.
     *  The GroupBox object (menu) and 3 push buttons. */
    QGroupBox   *m_p_gBMenu;
    QPushButton *m_p_pBConnect;
    QPushButton *m_p_pBDisconnect;
    QPushButton *m_p_pBQuit;

    /*  Device GroupBox variables declaration.
     *  The GroupBox object (device log) and a text browser object to display
     *  log messages. */
    QGroupBox    *m_p_gBDevice;
    QTextBrowser *m_p_tBDeviceLog;

    /*  Sensors GroupBox variables declaration.
     *  The GroupBox used to show each sensor data separately.
     *  */
    QGroupBox *m_p_gBSensors;

    /* Accelerometer data, X axis. */
    QLineEdit *m_p_lE_accel_x;
    QLineEdit *m_p_lE_accel_x_temp;
    QLineEdit *m_p_lE_vf_x_temp;

    /* Accelerometer data, Y axis. */
    QLineEdit *m_p_lE_accel_y;
    QLineEdit *m_p_lE_accel_y_temp;
    QLineEdit *m_p_lE_vf_y_temp;

    /* Accelerometer data, Z axis. */
    QLineEdit *m_p_lE_accel_z;
    QLineEdit *m_p_lE_accel_z_temp;
    QLineEdit *m_p_lE_vf_z_temp;

    /* Voltage data. */
    QLineEdit *m_p_lE_mdan_v5_ad;
    QLineEdit *m_p_lE_mdan_v5_reg;
    QLineEdit *m_p_lE_mdan_v15;
    QLineEdit *m_p_lE_mdan_vm15;

    /* Interrupt counters, from embedded code (SensorsB). */
    QLineEdit *m_p_lE_sensors_count;
    QLineEdit *m_p_lE_mdan_count;
    QLineEdit *m_p_lE_gyros_count;

    /* Gyrometer data, X axis. */
    QLineEdit *m_p_lE_gyro_x;
    QLineEdit *m_p_lE_gyro_x_temp;

    /* Gyrometer data, Y axis. */
    QLineEdit *m_p_lE_gyro_y;
    QLineEdit *m_p_lE_gyro_y_temp;

    /* Gyrometer data, Z axis. */
    QLineEdit *m_p_lE_gyro_z;
    QLineEdit *m_p_lE_gyro_z_temp;

    /* Time measured between each interrupt.*/
    QLineEdit *m_p_lE_mdan_time_ms;
    QLineEdit *m_p_lE_gyros_time_ms;

    /* A label for each one of the above LineEdit fields. */
    QLabel *m_p_L_voltage;
    QLabel *m_p_L_mdan;
    QLabel *m_p_L_gyros;
    QLabel *m_p_L_cycle;

    /* The serial device object. */
    MySerial m_serial_dev;

};

#endif // MAINWINDOW_H
