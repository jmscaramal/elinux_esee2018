#include "mainwindow.h"
#include "ui_mainwindow.h"

/* MainWindow class constructor. */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /* Initialize the Widgets*/
    createMenuGroupBox();
    createLogGroupBox();
    createSensorsGroupBox();

    /* Main Layout. Initializes a new Layout and adds the previous defined
     * widgets to it. */
    QVBoxLayout *mainLayout = new QVBoxLayout();

    mainLayout->addWidget(m_p_gBMenu);
    mainLayout->addWidget(m_p_gBDevice);
    mainLayout->addWidget(m_p_gBSensors);

    /* Set up central widget. Defines the window title, maximum size and adds
     * the above configured mainLayout to it. */
    setWindowTitle("FinalAPP V0.1");
    ui->centralWidget->setLayout(mainLayout);
    ui->centralWidget->setMaximumSize(1280,720);

    /* Define the buttons Connect and Disconnect to -> enabled. */
    m_p_pBConnect->setEnabled(true);
    m_p_pBDisconnect->setEnabled(true);

    /* Signal and Slots */
    /* Connects the action (signal) of clicking on the pushButton QUIT,
     * to the function (slot) close(), defined in this (MainWindow) class. */
    connect(m_p_pBQuit,
            SIGNAL(clicked()), this,
                               SLOT(close()));

    /* Connects the action (signal) of clicking on the pushButton Connect,
     * to the function (slot) openSerialPort(), defined in MySerial class. */
    connect(m_p_pBConnect,
            SIGNAL(clicked()), &m_serial_dev,
                                SLOT(openSerialPort()));

    /* Connects the action (signal) of clicking on the pushButton Disconnect,
     * to the function (slot) closeSerialPort(), defined in MySerial class. */
    connect(m_p_pBDisconnect,
            SIGNAL(clicked()), &m_serial_dev,
                                SLOT(closeSerialPort()));

    /* Connects the signal newData(sensors_t), defined in MySerial class,
     * to the function (slot) DataHandler(), defined in this (MainWindow) class.
     * Whenever the signal newData is sent from a MySerial object, the slot
     * DataHandler is ran. A sensors_t argument is sent with the signal and
     * passed to the slot function aswell. */
    connect(&m_serial_dev,
            SIGNAL(newData(sensors_t)), this,
                                        SLOT(DataHandler(sensors_t)));
}

/* Configures the MenuGroupBox widgets. */
void MainWindow::createMenuGroupBox()
{
    /* Sets the groupbox title and size. The tr() function is part of the Qt
     * internationalization system. It is used to show translated test strings
     * to systems which uses a different language. However, the QT does not
     * automatically translates such strings. The developer must define them.
     * See QtLinguist and Tr function documentation for more info. */
    m_p_gBMenu = new QGroupBox(tr("Main Menu"));
    m_p_gBMenu->setMaximumSize(500, 100);

    /* The QGridLayout class lays out widgets in a grid. It allows for
     * positioning the widgets in columns and rows coordinates. */
    QGridLayout *p_layout = new QGridLayout;

    /* Initializes the 3 pushbuttons and names them. */
    m_p_pBConnect       = new QPushButton(tr("Connect"), this);
    m_p_pBDisconnect    = new QPushButton(tr("Disconnect"), this);
    m_p_pBQuit          = new QPushButton(tr("Quit"), this);


    /* PushButtons gridlayout. Positions the buttons as follows:
     * pBConnect->      row 0, column 0;
     * pBDisconnect->   row 0, column 1;
     * pBQuit->         row 0, column 2;    */
    p_layout->addWidget(m_p_pBConnect,    0, 0);
    p_layout->addWidget(m_p_pBDisconnect, 0, 1);
    p_layout->addWidget(m_p_pBQuit,       0, 2);

    /* Sets the above defined settings to the groupboxMenu object. */
    m_p_gBMenu->setLayout(p_layout);
}

/* Configures the LogGroupBox widgets. Is the same process as the creation of
 * the above createMenuGroupBox(). */
void MainWindow::createLogGroupBox()
{
    m_p_gBDevice = new QGroupBox(tr("Device Log"));
    m_p_gBDevice->setMaximumSize(500, 400);

    QGridLayout *p_layout = new QGridLayout;

    m_p_tBDeviceLog = new QTextBrowser(this);
    m_p_tBDeviceLog->setMaximumSize(500, 700);

    /* PushButtons gridlayout. Here is a little difference regarding the
     * MenuGroupBox gridlayout. Besides defining the initial position of an
     * widget (row, column), it is also possible to define their respective
     * spans. That is, how many rows and columns, in terms of space, the widget
     * will ocup in the gridlayout. The following, positions the QTextBrowser
     * m_p_tBDeviceLog at row 0, column 0, and makes it spanning 3 rows and 3
     * columns.
     * Note: The size of each object is defined and divided automatically
     *       according to the groupBox maximumsize. However, the span feature
     *       can make a widget ocup more than 1 space (row or column or both).*/
    p_layout->addWidget(m_p_tBDeviceLog, 0, 0, 3, 3);

    m_p_gBDevice->setLayout(p_layout);

}

/* Configures the SensorsGroupBox widgets. Is the same process as the creation
 * of the above createMenuGroupBox() and createLogGroupBox(). */
void MainWindow::createSensorsGroupBox()
{
    m_p_gBSensors = new QGroupBox(tr("Sensory data"));
    m_p_gBSensors->setMaximumSize(500, 400);

    QGridLayout *p_layout = new QGridLayout;

    /* Initializes the QLineEdits (fields) with empty texts "". */
    m_p_lE_accel_x = new QLineEdit("", this);
    m_p_lE_accel_x_temp = new QLineEdit("", this);
    m_p_lE_vf_x_temp = new QLineEdit("", this);

    m_p_lE_accel_y = new QLineEdit("", this);
    m_p_lE_accel_y_temp = new QLineEdit("", this);
    m_p_lE_vf_y_temp = new QLineEdit("", this);

    m_p_lE_accel_z = new QLineEdit("", this);
    m_p_lE_accel_z_temp = new QLineEdit("", this);
    m_p_lE_vf_z_temp = new QLineEdit("", this);

    m_p_lE_mdan_v5_ad = new QLineEdit("", this);
    m_p_lE_mdan_v5_reg = new QLineEdit("", this);
    m_p_lE_mdan_v15 = new QLineEdit("", this);
    m_p_lE_mdan_vm15 = new QLineEdit("", this);

    m_p_lE_sensors_count = new QLineEdit("", this);
    m_p_lE_mdan_count = new QLineEdit("", this);
    m_p_lE_gyros_count = new QLineEdit("", this);

    m_p_lE_gyro_x = new QLineEdit("", this);
    m_p_lE_gyro_x_temp = new QLineEdit("", this);

    m_p_lE_gyro_y = new QLineEdit("", this);
    m_p_lE_gyro_y_temp = new QLineEdit("", this);

    m_p_lE_gyro_z = new QLineEdit("", this);
    m_p_lE_gyro_z_temp = new QLineEdit("", this);

    m_p_lE_gyros_time_ms = new QLineEdit("", this);
    m_p_lE_mdan_time_ms = new QLineEdit("", this);

    /* Initializes the labels and names them. */
    m_p_L_cycle =
            new QLabel(tr("Cycle (Int, Mdan, Gyros) and times (Mdan, Gyros):"));

    m_p_L_voltage =
            new QLabel(tr("Voltage (5V_AD, 5V_REG, 15V, -15V):"));

    m_p_L_mdan =
            new QLabel(("MDAN Sensors (Accel, Accel_tmp, VF_tmp) (X, Y, Z):"));

    m_p_L_gyros = new QLabel(("Gyro Sensors (Gyro, Gyro_tmp) (X, Y, Z):"));


    /* Sensors layout. Positions each QLineEdit and the above labels. */

    /*Cycles and time*/
    /* Ex: Puts the label m_p_L_cycle at row 0 and column 0, and makes it
     * ocupping 1 row and 5 columns. */
    p_layout->addWidget(m_p_L_cycle, 0, 0, 1, 5);
    p_layout->addWidget(m_p_lE_sensors_count, 1, 0);
    p_layout->addWidget(m_p_lE_mdan_count, 1, 1);
    p_layout->addWidget(m_p_lE_gyros_count, 1, 2);
    p_layout->addWidget(m_p_lE_mdan_time_ms, 1, 3);
    p_layout->addWidget(m_p_lE_gyros_time_ms, 1, 4);


    /*Voltage values */
    p_layout->addWidget(m_p_L_voltage, 2, 0, 1, 5);
    p_layout->addWidget(m_p_lE_mdan_v5_ad, 3, 0);
    p_layout->addWidget(m_p_lE_mdan_v5_reg, 3, 1);
    p_layout->addWidget(m_p_lE_mdan_v15, 3, 2);
    p_layout->addWidget(m_p_lE_mdan_vm15, 3, 3);

    /* Mdan accels and VFs*/
    p_layout->addWidget(m_p_L_mdan, 4, 0, 1, 5);
    p_layout->addWidget(m_p_lE_accel_x, 5, 0);
    p_layout->addWidget(m_p_lE_accel_x_temp, 5, 1);
    p_layout->addWidget(m_p_lE_vf_x_temp, 5, 2);

    p_layout->addWidget(m_p_lE_accel_y, 6, 0);
    p_layout->addWidget(m_p_lE_accel_y_temp, 6, 1);
    p_layout->addWidget(m_p_lE_vf_y_temp, 6, 2);

    p_layout->addWidget(m_p_lE_accel_z, 7, 0);
    p_layout->addWidget(m_p_lE_accel_z_temp, 7, 1);
    p_layout->addWidget(m_p_lE_vf_z_temp, 7, 2);

    /* Gyros */
    p_layout->addWidget(m_p_L_gyros, 8, 0, 1, 5);
    p_layout->addWidget(m_p_lE_gyro_x, 9, 0);
    p_layout->addWidget(m_p_lE_gyro_x_temp, 9, 1);

    p_layout->addWidget(m_p_lE_gyro_y, 10, 0);
    p_layout->addWidget(m_p_lE_gyro_y_temp, 10, 1);

    p_layout->addWidget(m_p_lE_gyro_z, 11, 0);
    p_layout->addWidget(m_p_lE_gyro_z_temp, 11, 1);

    m_p_gBSensors->setLayout(p_layout);
}

/* This function puts each data present on the "sensors" argument to its
 * respective field in SensorsGroupBox. */
void MainWindow::DataHandler(sensors_t sensors)
{
    /* Puts the following string + the sensors.count converted to text string
     * in the Device Log TextBrowser.*/
    m_p_tBDeviceLog->append(
                "Received packet: " + QString::number(sensors.count));

    /* Cycles and Time */
    m_p_lE_sensors_count->setText(QString::number(sensors.count));
    m_p_lE_mdan_count->setText(QString::number(sensors.mdan.count));
    m_p_lE_gyros_count->setText(QString::number(sensors.gyros.count));
    m_p_lE_mdan_time_ms->setText(QString::number(sensors.mdan.time_ms));
    m_p_lE_gyros_time_ms->setText(QString::number(sensors.gyros.time_ms));

    /*Voltage values */
    m_p_lE_mdan_v5_ad->setText(QString::number(sensors.mdan.v_ad_5));;
    m_p_lE_mdan_v5_reg->setText(QString::number(sensors.mdan.v_reg_5));;
    m_p_lE_mdan_v15->setText(QString::number(sensors.mdan.v_ad_15));;
    m_p_lE_mdan_vm15->setText(QString::number(sensors.mdan.v_ad_m15));;

    /* Mdan accels and VFs*/
    m_p_lE_accel_x->setText(QString::number(sensors.mdan.accel_x));;
    m_p_lE_accel_x_temp->setText(QString::number(sensors.mdan.accel_x_temp));;
    m_p_lE_vf_x_temp->setText(QString::number(sensors.mdan.vf_x_temp));;

    m_p_lE_accel_y->setText(QString::number(sensors.mdan.accel_y));;
    m_p_lE_accel_y_temp->setText(QString::number(sensors.mdan.accel_y_temp));;
    m_p_lE_vf_y_temp->setText(QString::number(sensors.mdan.vf_y_temp));;

    m_p_lE_accel_z->setText(QString::number(sensors.mdan.accel_z));;
    m_p_lE_accel_z_temp->setText(QString::number(sensors.mdan.accel_z_temp));;
    m_p_lE_vf_z_temp->setText(QString::number(sensors.mdan.vf_z_temp));;

    /* Gyros */
    m_p_lE_gyro_x->setText(QString::number(sensors.gyros.gyro_x));;
    m_p_lE_gyro_x_temp->setText(QString::number(sensors.gyros.gyro_x_temp));;

    m_p_lE_gyro_y->setText(QString::number(sensors.gyros.gyro_y));;
    m_p_lE_gyro_y_temp->setText(QString::number(sensors.gyros.gyro_y_temp));;

    m_p_lE_gyro_z->setText(QString::number(sensors.gyros.gyro_z));;
    m_p_lE_gyro_z_temp->setText(QString::number(sensors.gyros.gyro_z_temp));;

}

/* MainWindow descructor. Closes the serial port and deletes the user interface.
 */
MainWindow::~MainWindow()
{
    m_serial_dev.closeSerialPort();
    delete ui;    
}
