/*
 * mdan_task.c
 *
 *  Created on: 07/07/2018
 *      Author: scaramal
 */

//==============================================================================
//                              USED INTERFACES
//==============================================================================

#include <mqueue.h>
#include <pthread.h>
#include <time.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include "defines.h"
#include "structs.h"

//==============================================================================
//                        MACROS AND DATATYPE DEFINITIONS
//==============================================================================

//==============================================================================
//                     STATIC (PRIVATE) FUNCTION PROTOTYPES
//==============================================================================

//==============================================================================
//                          STATIC GLOBAL VARIABLES
//==============================================================================

//==============================================================================
//                      IMPLEMENTATION OF PUBLIC FUNCTIONS
//==============================================================================

void * mdan_task(void *p_arg)
{
	/* The queue to share messages with the main process. */
	mqd_t main_queue;

	mdan_t mdan;

	/* Variables used to measure the processor ticks and to compute the spent
	 * time between the measures. */
	static struct timespec tm_start_int, tm_stop_int;

	/* A file descriptor which is linked to the file containing the logged
	 * sensory data. */
	FILE *p_mdan_fd;

	/* Opens the above file for reading. ('r') */
	p_mdan_fd = fopen(MDAN_LOG, "r");

	if (p_mdan_fd == NULL)
	{
		printf("error mdan_log_file\n");
	}

	/* A readings counter. */
	uint32_t mdan_count = 0;

	/* A variable to store the measured time between each */
	float time_ms;

	/* The same process as the one in main function. See the comments there. */
	struct mq_attr attr;

	attr.mq_flags = 0;
	attr.mq_maxmsg = 10;
	attr.mq_msgsize = QUEUE_MSG_SIZE;
	attr.mq_curmsgs = 0;

	/* The same process as the one in main function. The difference here,
	 * however, relies on the suppression of the O_CREAT flag, which implies in
	 * an attempt of opening an already existing queue (the main function must
	 * create it before this call). */
	main_queue = mq_open(MDAN_QUEUE_NAME, O_WRONLY, S_IRUSR | S_IWUSR, &attr);

	if (main_queue < 0)
	{
		printf("Erro opening main queue (mdan task)\n");
	}

	if (VERBOSE)
	{
		printf("Mdan task initialized!\n");
	}

	/* Sets the initial mark to count the number of ticks between each reading.
	 * */
	clock_gettime(CLOCK_MONOTONIC, &tm_start_int);

	/* Instead of using a "while(1)" call, the following 'while' verifies the
	 * mdan log file contents, that is, while there are data to be read...
	 *
	 * The fread call, reads '1' (3rd argument) x "sizeof(mdan_t)" bytes from
	 * the p_mdan_fd file, and saves the read bytes into the mdan variable.
	 *  */
	while(fread(&mdan, sizeof(mdan_t), 1, p_mdan_fd))
	{
		/* The pthread_mutex_lock call waits for the mutex mut and locks it as
		 * soon as it is available.*/
		pthread_mutex_lock(&mut);

		/* Then, the pthread_cond_wait call unlocks the mutex and put this
		 * thread, mdan_task, to sleep. It will wait for a signal in the cond
		 * variable. Then, as soon as it receives a signal (from the main
		 * process (pthread_cond_broadcast)), the thread wakes up and locks the
		 * mut variable again.  */
		pthread_cond_wait(&cond, &mut);

		/* This space, between pthread_cond_wait and pthread_mutex_unlock,
		 * should be used to edit some shared resources between both threads,
		 * the one waiting for a signal and the sender. This is usually used
		 * to avoid constantly checking for such resource, that is, the cond
		 * variable informs (signalizes) the thread that, the resource it was
		 * waiting for, is available. */

		/* In this particular case, we are only interested in the thread
		 * synchronism, since the operations of pthread_cond_wait, signal, mutex,
		 * are all atomic.*/
		pthread_mutex_unlock(&mut);

		/* Sets the final mark to count the number of ticks between each reading.
		 * */
		clock_gettime(CLOCK_MONOTONIC, &tm_stop_int);

		/* Computes the time spent between each reading (interrupt), in ms,
		 * based on the number of read ticks. */
		time_ms = (tm_stop_int.tv_sec - tm_start_int.tv_sec) * 1000
				+ ((tm_stop_int.tv_nsec - tm_start_int.tv_nsec) * 0.001)*0.001;

		/* Updates the initial mark with the last one. */
		tm_start_int = tm_stop_int;

		mdan_count++;

		mdan.count   = mdan_count;
		mdan.time_ms = time_ms;

		/* Sends the read data, stored in the mdan variable, to the main thread.
		 * */
		if ( mq_send(main_queue, (char*)&mdan, QUEUE_MSG_SIZE, 0) < 0 )
		{
			printf("error when sending from mdan_task to main\n");
		}

	}

	/* Closes the log file descriptor. */
	fclose(p_mdan_fd);

	/* Close (frees) the message queue file descriptors. */
	mq_close(main_queue);
	/* Deletes the created queues, including their current messages. */
	mq_unlink(MDAN_QUEUE_NAME);
	/* Finalizes the thread. */
	pthread_exit(NULL);
}

//==============================================================================
//                IMPLEMENTATION OF STATIC (PRIVATE) FUNCTIONS
//==============================================================================

