/*
 * my_defines.h
 *
 *  Created on: 29/06/2018
 *      Author: scaramal
 */

#ifndef MY_DEFINES_H_
#define MY_DEFINES_H_


/* HW and Interfaces */

#define SERIAL 	"/dev/ttyS0"
#define SERIAL_BAUD 115200

#define LED0 	"/sys/class/leds/led0/brightness"	// Green Led
#define LED1	"/sys/class/leds/led1/brightness"	// Red Led

#define LED_ON		"1"
#define LED_OFF		"0"

#define TIME_MS	1000000		// Value in microseconds units.



#endif /* MY_DEFINES_H_ */
