/*
 ============================================================================
 Name        : MyFirstApp.c
 Author      : jms
 Version     :
 Copyright   : Your copyright notice
 Description : A simple C program for demonstrating the PIGPIO Library interface.
 ============================================================================
 */

//==============================================================================
//                              USED INTERFACES
//==============================================================================

#define _GNU_SOURCE             /* See feature_test_macros(7) */

#include <fcntl.h>				/* Required by open / write system calls.     */
#include <pthread.h>			/* POSIX threads Library. Required by PIGPIO. */
#include <stdint.h>				/* Standard Int Library.(uint32_t, uint8_t...)*/
#include <stdio.h>				/* Standard Input/Ouput Library. (printf...)  */
#include <stdlib.h>				/* General (standard) Library. (EXIT_SUCCESS. */
#include <termios.h>			/* Terminal Input/Output interfaces.          */
#include <unistd.h>				/* Complement (flags, constants, definitions..)
									for the POSIX API. */

#include "pigpio.h"				/* PIGPIO Library header. */

//==============================================================================
//                        MACROS AND DATATYPE DEFINITIONS
//==============================================================================

/* HW and Interfaces */

#define SERIAL 	"/dev/ttyS0"
#define SERIAL_BAUD 115200

#define LED0 	"/sys/class/leds/led0/brightness"	// Green Led
#define LED1	"/sys/class/leds/led1/brightness"	// Red Led

#define LED_ON		"1"
#define LED_OFF		"0"

#define TIME_MS	1000000		// Value in microseconds units.

//==============================================================================
//                     STATIC (PRIVATE) FUNCTION PROTOTYPES
//==============================================================================

/**
 * @brief A function to verify if any key was pressed on terminal. Adapted from
 *
 *  https://cboard.cprogramming.com/c-programming/63166-kbhit-linux.html
 *
 *
 * @return TRUE     If a key was pressed.
 * @return FALSE    Otherwise.
 */
static int kbhit(void);


/**
 * @brief The callback function attached to the GPIO pin interrupt. The bellow
 * 		  parameters are provided by the RPiGPIO Library.
 *
 * @param[in] gpio     0-53     The GPIO which has changed state.
 * @param[in] level    0-2      0 = change to low (a falling edge)
 *		                        1 = change to high (a rising edge)
 *           		            2 = no level change (interrupt timeout)
 * @param[in] tick     32 bit   The number of microseconds since boot
 *
 */
static void signal_handler_IO(
		int gpio,
		int level,
		uint32_t tick);

/**
 * @brief Initializes the p_path serial device. Opens the serial device and
 * 		  sets minimal attributes to get it working.
 *
 * @param[in] p_path    The serial device path.
 * @param[out] p_fd     A file descriptor to link to the serial device above.
 * @param[in] speed     The serial BAUDRATE.
 *
 * @return  1   If the serial device was initialized.
 * @return  0   If an error has occurred.
 */
uint8_t serial_initialize(
	const char *p_path,
	int32_t *p_fd,
	const uint32_t speed);


//==============================================================================
//                          STATIC GLOBAL VARIABLES
//==============================================================================

uint32_t tick_spent;		/* A variable used to store the number of occurred
								ticks between each GPIO interrupt. */

//==============================================================================
//                      IMPLEMENTATION OF PUBLIC FUNCTIONS
//==============================================================================

int main(void)
{
	int   led0_fd;			/* A file descriptor to manipulate the LED0.  */
	int   led1_fd;			/* A file descriptor to manipulate the LED1.  */
	int serial_fd;			/* A file descriptor to manipulate the SERIAL.*/
	uint8_t serial_st;		/* A status for the serial_initialize func.   */

	tick_spent = 0;

	if (gpioInitialise() < 0) /* Such function must be called before using the
								 PIGPIO library.*/
	{
		fprintf(stderr, "pigpio initialization failed\n");
		return 1;
	}

	led0_fd   = open(LED0  , O_RDWR);			/* Opens LED0 for reading and writing.*/
    led1_fd   = open(LED1  , O_RDWR);			/* Opens LED1 for reading and writing.*/

    /* Opens SERIAL for reading and writing at SERIAL_BAUD baudrate.*/
    serial_st = serial_initialize(SERIAL, &serial_fd, SERIAL_BAUD);


    if (led0_fd < 0)
	{
		printf("Error: Failed to open serial %s \n\n", LED0);
	}

    if (led1_fd < 0)
	{
		printf("Error: Failed to open serial %s \n\n", LED1);
	}

    if (serial_st == 0)
	{
		printf("Error: Failed to open serial %s \n\n", SERIAL);
	}

    if (gpioSetMode(23, PI_OUTPUT) != 0)
    {
    	printf("Error: Failed to set GPIO %d \n\n", 23);
    }

    if (gpioSetMode(16, PI_OUTPUT) != 0)
    {
		printf("Error: Failed to set GPIO %d \n\n", 16);
	}

    if (gpioSetMode(20, PI_INPUT) != 0)
	{
		printf("Error: Failed to set GPIO %d \n\n", 20);
	}

    /**
     * Registers the signal_handler function to be called
     * (a callback) whenever an interrupt occurs on
     * the specified GPIO (20). The EITHER_EDGE parameter
     * specifies an interrupt for both: Rising or Falling
     * edges. The parameter 100 indicates a 100ms timeout.
     */
    gpioSetISRFunc(20, EITHER_EDGE, 100, signal_handler_IO);
    /* PS: If you want to see a result different from the timeout,
     * i.e., a detected RISING or FALLING edge, you should connect
     * the GPIO pins (BCM) 20(input)-23(pwm). Which physically are:
     * the pins 38 and 16, respectively. */

    int freq_ret;

    /* Sets a 50Hz PWM Frequency for the GPIO  23 pin.
     * It returns the numerically closest frequency if OK.
     * The frequency depends on other factors, please for
     * further details check:
     * http://abyz.me.uk/rpi/pigpio/cif.html#gpioSetPWMfrequency*/
    freq_ret = gpioSetPWMfrequency(23, 50);

    printf("Freq: %d \n\n", freq_ret);

    /* Selects the duty-cycle range to be used for the GPIO.
     * Subsequent calls to gpioPWM will use a duty-cycle
     * between 0 (off) and range (fully on).]
     * the range value can be: 25-40000 */
    gpioSetPWMrange(23, 1000);

    /* According to the above specified range (1000), sets a duty-cicle
     * of 50%. */
    gpioPWM(23, 500);

	while(!kbhit())	/* While a key from the keyboard isn't pressed... */
	{
		write(serial_fd, "LED 0 -> ON, LED 1 -> OFF\n", sizeof("LED 0 -> ON, LED 1 -> OFF\n"));
		write(led0_fd  ,  LED_ON , sizeof(LED_ON));
		write(led1_fd  ,  LED_OFF, sizeof(LED_OFF));

		printf("LED 0 -> ON, LED 1 -> OFF\n");

		gpioDelay(TIME_MS);

		write(serial_fd, "LED 1 -> ON, LED 0 -> OFF\n", sizeof("LED 1 -> ON, LED 0 -> OFF\n"));
		write(led1_fd  , LED_ON , sizeof(LED_ON));
		write(led0_fd  , LED_OFF, sizeof(LED_OFF));

		printf("LED 1 -> ON, LED 0 -> OFF\n");
		gpioDelay(TIME_MS);


	}

	write(led0_fd, LED_OFF, sizeof(LED_OFF));
	write(led1_fd, LED_OFF, sizeof(LED_OFF));

	close(led0_fd);
	close(led1_fd);
	close(serial_fd);

	/* Similar to the gpioInitialise function, this one must be called at the end of
	 * your program.*/
	gpioTerminate();

	return EXIT_SUCCESS;
}

//==============================================================================
//                IMPLEMENTATION OF STATIC (PRIVATE) FUNCTIONS
//==============================================================================

static int kbhit(void)
{
    struct termios oldt, newt;

    int ch;
    int oldf;

    /** Gets the attributes from the standard input file descriptor. */
    tcgetattr(STDIN_FILENO, &oldt);

    /** Enables raw input. (unprocessed). */
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);

    /** Sets the new attributes. */
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    /** Gets the file access mode and status flags. */
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);

    /** Sets the same above read values + the O_NONBLOCK flag. */
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    /** Checks if there is a char on the standard terminal. (nonblock). */
    ch = getchar();

    /** Make changes now, without waiting for data to complete. */
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

    /** Removes the O_NONBLOCK flag. */
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    /** If any key was hitted, return true. And puts back the pressed key onto
     *  the terminal. */
    if(ch != EOF)
    {
        ungetc(ch, stdin);
        return 1;
    }

    return 0;
}

uint8_t serial_initialize(
	const char *p_path,
	int32_t *p_fd,
	const uint32_t speed)
{

	uint8_t ret;

    /** Tries to open the serial device (p_path).
     *  O_RDWR:     Request read and write permissions. */
	*p_fd = open(p_path, O_RDWR);

	if (p_fd < 0)
	{
		printf("Error: Failed to open serial %s \n\n", p_path);

		ret = 0;
	}
	else
	{
		printf("Serial (%s) opened!!\n\n", p_path);

		/** The structure to set and get the device parameters. */
		struct termios config;

		/** Get the current configuration of the serial interface. */
		if (tcgetattr(*p_fd, &config) < 0)
		{
			printf("Error: Could not get the %s attributes!\n\n", p_path);
		}
		else
		{
			printf("Serial (%s) attributes read.. \n\n", p_path);

			/** Check if the fd is pointing to a TTY device or not. */
			if (!isatty(*p_fd))
			{
				printf("Error: %s isnt pointing to a TTY device!!\n\n", p_path);
			}
			else
			{
				printf("%s is a tty device. Continuing...\n\n", p_path);

				/** Setting the baudrate. */
				if (cfsetspeed(&config, (speed_t)speed) < 0)
				{
					printf("Error: Couldn't set the %s baudrate!!\n\n", p_path);
				}
				else
				{
					/** No parity. */
					config.c_cflag     &=  ~PARENB;
					/** One stop bit. */
					config.c_cflag     &=  ~CSTOPB;
					/** Zeroes the char size mask. */
					config.c_cflag     &=  ~CSIZE;
					/** Sets data size = 8 bits. */
					config.c_cflag     |=  CS8;
					/** Disables HW flow control. */
					config.c_cflag     &=  ~CRTSCTS;
					/** Minimum number of characters for read. */
					config.c_cc[VMIN]   =  1;
					/** Timeout in deciseconds for read. (0.5s) */
					config.c_cc[VTIME]  =  5;
					/** Enables READ and ignores control lines. */
					config.c_cflag     |=  CREAD | CLOCAL;

					/** Set the terminal to "raw mode". */
					cfmakeraw(&config);

				    /** Flushes the serial dev and sets the new attributes. */
    				if (tcsetattr(*p_fd, TCSAFLUSH, &config) < 0)
    				{
    					printf("Error: Couldn't set the %s attributes!!\n\n",
                            p_path);
    				}
    				else
    				{
    					ret = 1;
    				}
				}
			}
		}
	}
	return ret;
}

void signal_handler_IO(int gpio, int level, uint32_t tick)
{
	/* Reads the current state of the GPIO pin 16. */
	int gpio_val = gpioRead(16);

	/* Inverts such state. */
	gpio_val = !gpio_val;

	/* Updates the gpio pin's state. */
	gpioWrite(16, gpio_val);

	/* Computes and shows the number of spent ticks between the last and the current interrupt. */
	printf("Interrupt Occurred! Level-> %d\t tick-> %d\n",level, tick-tick_spent);
	tick_spent = tick;
}


